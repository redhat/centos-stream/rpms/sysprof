From 6e1f5d336fde126e30011cabccfbca555e075ac5 Mon Sep 17 00:00:00 2001
From: =?UTF-8?q?Barnab=C3=A1s=20P=C5=91cze?= <pobrn@protonmail.com>
Date: Wed, 9 Oct 2024 15:28:14 +0200
Subject: [PATCH 03/33] libsysprof: elf: do not generate fallback names

Fallback names are only used in `SysprofElfSymbolizer` at the
moment, but it also has code to generate fallback symbols.
---
 src/libsysprof/sysprof-elf-private.h    |  3 +--
 src/libsysprof/sysprof-elf-symbolizer.c | 12 ++++++------
 src/libsysprof/sysprof-elf.c            | 17 +++++------------
 3 files changed, 12 insertions(+), 20 deletions(-)

diff --git a/src/libsysprof/sysprof-elf-private.h b/src/libsysprof/sysprof-elf-private.h
index b4d0f737..146bc2de 100644
--- a/src/libsysprof/sysprof-elf-private.h
+++ b/src/libsysprof/sysprof-elf-private.h
@@ -42,8 +42,7 @@ const char *sysprof_elf_get_debug_link        (SysprofElf   *self);
 char       *sysprof_elf_get_symbol_at_address (SysprofElf   *self,
                                                guint64       address,
                                                guint64      *begin_address,
-                                               guint64      *end_address,
-                                               gboolean     *is_fallback);
+                                               guint64      *end_address);
 SysprofElf *sysprof_elf_get_debug_link_elf    (SysprofElf   *self);
 void        sysprof_elf_set_debug_link_elf    (SysprofElf   *self,
                                                SysprofElf   *debug_link_elf);
diff --git a/src/libsysprof/sysprof-elf-symbolizer.c b/src/libsysprof/sysprof-elf-symbolizer.c
index 8fdf204f..f7aabef7 100644
--- a/src/libsysprof/sysprof-elf-symbolizer.c
+++ b/src/libsysprof/sysprof-elf-symbolizer.c
@@ -61,9 +61,9 @@ sysprof_elf_symbolizer_symbolize (SysprofSymbolizer        *symbolizer,
   g_autoptr(SysprofElf) elf = NULL;
   SysprofDocumentMmap *map;
   g_autofree char *name = NULL;
+  const char *nick = NULL;
   const char *path;
   const char *build_id;
-  gboolean is_fallback = FALSE;
   guint64 map_begin;
   guint64 map_end;
   guint64 relative_address;
@@ -115,14 +115,15 @@ sysprof_elf_symbolizer_symbolize (SysprofSymbolizer        *symbolizer,
                                        NULL)))
     goto fallback;
 
+  nick = sysprof_elf_get_nick (elf);
+
   /* Try to get the symbol name at the address and the begin/end address
    * so that it can be inserted into our symbol cache.
    */
   if (!(name = sysprof_elf_get_symbol_at_address (elf,
                                                   relative_address,
                                                   &begin_address,
-                                                  &end_address,
-                                                  &is_fallback)))
+                                                  &end_address)))
     goto fallback;
 
   /* Sanitize address ranges if we have to. Sometimes that can happen
@@ -135,11 +136,10 @@ sysprof_elf_symbolizer_symbolize (SysprofSymbolizer        *symbolizer,
 
   ret = _sysprof_symbol_new (sysprof_strings_get (strings, name),
                              sysprof_strings_get (strings, path),
-                             sysprof_strings_get (strings, sysprof_elf_get_nick (elf)),
+                             sysprof_strings_get (strings, nick),
                              map_begin + (begin_address - file_offset),
                              map_begin + (end_address - file_offset),
                              SYSPROF_SYMBOL_KIND_USER);
-  ret->is_fallback = is_fallback;
 
   return ret;
 
@@ -156,7 +156,7 @@ fallback:
 
   ret = _sysprof_symbol_new (sysprof_strings_get (strings, name),
                              sysprof_strings_get (strings, path),
-                             NULL,
+                             sysprof_strings_get (strings, nick),
                              begin_address, end_address,
                              SYSPROF_SYMBOL_KIND_USER);
   ret->is_fallback = TRUE;
diff --git a/src/libsysprof/sysprof-elf.c b/src/libsysprof/sysprof-elf.c
index 0534eccb..df73ce6d 100644
--- a/src/libsysprof/sysprof-elf.c
+++ b/src/libsysprof/sysprof-elf.c
@@ -409,8 +409,7 @@ sysprof_elf_get_symbol_at_address_internal (SysprofElf *self,
                                             guint64     address,
                                             guint64    *begin_address,
                                             guint64    *end_address,
-                                            guint64     text_offset,
-                                            gboolean   *is_fallback)
+                                            guint64     text_offset)
 {
   const ElfSym *symbol;
   char *ret = NULL;
@@ -421,7 +420,7 @@ sysprof_elf_get_symbol_at_address_internal (SysprofElf *self,
 
   if (self->debug_link_elf != NULL)
     {
-      ret = sysprof_elf_get_symbol_at_address_internal (self->debug_link_elf, filename, address, begin_address, end_address, text_offset, is_fallback);
+      ret = sysprof_elf_get_symbol_at_address_internal (self->debug_link_elf, filename, address, begin_address, end_address, text_offset);
 
       if (ret != NULL)
         return ret;
@@ -447,11 +446,7 @@ sysprof_elf_get_symbol_at_address_internal (SysprofElf *self,
     }
   else
     {
-      begin = address;
-      end = address + 1;
-      ret = g_strdup_printf ("In File %s+0x%"G_GINT64_MODIFIER"x", filename, address);
-      if (is_fallback)
-        *is_fallback = TRUE;
+      return NULL;
     }
 
   if (begin_address)
@@ -467,16 +462,14 @@ char *
 sysprof_elf_get_symbol_at_address (SysprofElf *self,
                                    guint64     address,
                                    guint64    *begin_address,
-                                   guint64    *end_address,
-                                   gboolean   *is_fallback)
+                                   guint64    *end_address)
 {
   return sysprof_elf_get_symbol_at_address_internal (self,
                                                      self->file,
                                                      address,
                                                      begin_address,
                                                      end_address,
-                                                     self->text_offset,
-                                                     is_fallback);
+                                                     self->text_offset);
 }
 
 /**
-- 
2.45.2

