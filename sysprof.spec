%global glib2_version 2.76.0
%global tarball_version %%(echo %{version} | tr '~' '.')
%global bundled_libunwind %{defined rhel}

%if 0%{?bundled_libunwind}
%global libunwind_version 1.8.1
%global _legacy_common_support 1
%endif

Name:           sysprof
Version:        47.2
Release:        %autorelease
Summary:        A system-wide Linux profiler

License:        GPL-2.0-or-later AND GPL-3.0-or-later AND CC-BY-SA-4.0 AND BSD-2-Clause-Patent
URL:            http://www.sysprof.com
Source0:        https://download.gnome.org/sources/sysprof/47/sysprof-%{tarball_version}.tar.xz
%if 0%{?bundled_libunwind}
Source1:        https://github.com/libunwind/libunwind/releases/download/v%{libunwind_version}/libunwind-%{libunwind_version}.tar.gz
%endif

# Backports of debuginfod and sysprof-live-unwinder from GNOME 48
Patch:          0001-build-add-47-version-macros.patch
Patch:          0002-libsysprof-elf-do-not-allow-setting-self-as-debug-li.patch
Patch:          0003-libsysprof-elf-do-not-generate-fallback-names.patch
Patch:          0004-sysprof-update-to-AdwSpinner.patch
Patch:          0005-sysprof-add-SysprofDocumentTask-abstraction.patch
Patch:          0006-libsysprof-add-setup-hooks-for-symbolizers.patch
Patch:          0007-libsysprof-hoist-fallback-symbol-creation.patch
Patch:          0008-libsysprof-add-debuginfod-symbolizer.patch
Patch:          0009-libsysprof-ensure-access-to-process-info.patch
Patch:          0010-libsysprof-fix-building-with-Ddebuginfod-auto.patch
Patch:          0011-libsysprof-return-NULL-instance-unless-debuginfod-wo.patch
Patch:          0012-build-always-build-debuginfod-symbolizer.patch
Patch:          0013-libsysprof-remove-unnecessary-address-calculation.patch
Patch:          0014-libsysprof-add-muxer-GSource.patch
Patch:          0015-libsysprof-add-support-for-stack-regs-options-in-att.patch
Patch:          0016-sysprofd-add-support-for-unwinding-without-frame-poi.patch
Patch:          0017-libsysprof-add-SysprofUserSampler-for-live-unwinding.patch
Patch:          0018-sysprof-cli-add-support-for-live-unwinding.patch
Patch:          0019-sysprof-add-UI-for-live-unwinding.patch
Patch:          0020-sysprof-live-unwinder-ifdef-unused-code-off-x86.patch
Patch:          0021-sysprof-only-show-user-stack-sampling-on-x86.patch
Patch:          0022-libsysprof-check-for-PERF_REG_EXTENDED_MASK-availabi.patch
Patch:          0023-sysprof-live-unwinder-fix-source-func-prototype.patch
Patch:          0024-sysprof-live-unwinder-handle-large-stack-unwind-size.patch
Patch:          0025-unwinder-wait-for-completion-of-subprocess.patch
Patch:          0026-sysprof-user-sampler-implement-await-for-FDs.patch
Patch:          0027-libsysprof-provide-unwind-pipe-from-client.patch
Patch:          0028-sysprof-live-unwinder-error-out-on-capture-failure.patch
Patch:          0029-sysprofd-remove-unused-code.patch
Patch:          0030-build-lower-libpanel-requirement-to-ease-some-build-.patch
Patch:          0031-build-allow-await-for-FD-with-older-libdex.patch
Patch:          0032-sysprof-default-stack-capturing-as-enabled.patch
Patch:          0033-tests-fix-test-on-s390.patch

BuildRequires:  gcc
BuildRequires:  gcc-c++
BuildRequires:  gettext
BuildRequires:  itstool
BuildRequires:  meson
BuildRequires:  pkgconfig(gio-2.0) >= %{glib2_version}
BuildRequires:  pkgconfig(gio-unix-2.0) >= %{glib2_version}
BuildRequires:  pkgconfig(glib-2.0) >= %{glib2_version}
BuildRequires:  pkgconfig(gobject-2.0)
BuildRequires:  pkgconfig(gtk4)
BuildRequires:  pkgconfig(json-glib-1.0)
BuildRequires:  pkgconfig(libadwaita-1)
BuildRequires:  pkgconfig(libdex-1)
BuildRequires:  pkgconfig(libpanel-1)
BuildRequires:  pkgconfig(libsystemd)
BuildRequires:  pkgconfig(polkit-gobject-1)
BuildRequires:  pkgconfig(systemd)
BuildRequires:  pkgconfig(libdw)
BuildRequires:  pkgconfig(libdebuginfod)
BuildRequires:  /usr/bin/appstream-util
BuildRequires:  /usr/bin/desktop-file-validate

%if 0%{?bundled_libunwind}
BuildRequires:  automake libtool autoconf make
%else
BuildRequires:  pkgconfig(libunwind-generic)
%endif

Requires:       glib2%{?_isa} >= %{glib2_version}
Requires:       hicolor-icon-theme
Requires:       %{name}-cli%{?_isa} = %{version}-%{release}

%description
Sysprof is a sampling CPU profiler for Linux that collects accurate,
high-precision data and provides efficient access to the sampled
calltrees.


%package        agent
Summary:        Sysprof agent utility

%description    agent
The %{name}-agent package contains the sysprof-agent program. It provides a P2P
D-Bus API to the process which can control subprocesses. It's used by IDE
tooling to have more control across container boundaries.


%package        cli
Summary:        Sysprof command line utility
# sysprofd needs turbostat
Requires:       kernel-tools
Requires:       libsysprof%{?_isa} = %{version}-%{release}

%description    cli
The %{name}-cli package contains the sysprof-cli command line utility.


%package     -n libsysprof
Summary:        Sysprof libraries
# Subpackage removed/obsoleted in F39
Obsoletes:      libsysprof-ui < 45.0
%if 0%{?bundled_libunwind}
Provides:       bundled(libunwind) = %{libunwind_version}
%endif

%description -n libsysprof
The libsysprof package contains the Sysprof libraries.


%package        capture-devel
Summary:        Development files for sysprof-capture static library
License:        BSD-2-Clause-Patent
Provides:       sysprof-capture-static = %{version}-%{release}

%description    capture-devel
The %{name}-capture-devel package contains the sysprof-capture static library and header files.


%package        devel
Summary:        Development files for %{name}
Requires:       %{name}-capture-devel%{?_isa} = %{version}-%{release}
Requires:       libsysprof%{?_isa} = %{version}-%{release}

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%prep
%if 0%{?bundled_libunwind}
%setup -b 1 -n libunwind-%{libunwind_version}
%endif
%setup -n sysprof-%{tarball_version}
%autopatch -p1


%build
%if 0%{?bundled_libunwind}
# First build private libunwind
%global libunwind_install_dir %{buildroot}%{_builddir}/libunwind
pushd ../libunwind-%{libunwind_version}
mkdir -p %{_builddir}/libunwind/
aclocal
libtoolize --force
autoheader
automake --add-missing
autoconf
%configure --enable-static --disable-shared --enable-setjmp=no --disable-debug --disable-documentation --disable-ptrace --disable-coredump --disable-minidebuginfo --disable-zlibdebuginfo --with-pic
make %{?_smp_mflags} install DESTDIR=%{libunwind_install_dir}
popd
# Our "/usr" install to DESTDIR wont get picked up by the
# pkgconfig use in meson so ensure access to those include
# and linker directories manually.
export CFLAGS="$CFLAGS -I%{libunwind_install_dir}/usr/include"
export LDFLAGS="$LDFLAGS -L%{libunwind_install_dir}/usr/%{_lib}"
%global pkg_config_path_override --pkg-config-path %{libunwind_install_dir}/usr/%{_lib}/pkgconfig
%endif

# Now build sysprof
%meson %{?pkg_config_path_override}
%meson_build


%install
%meson_install
%find_lang %{name} --with-gnome
%if 0%{?bundled_libunwind}
# Appease checks which would include buildroot paths for the
# libunwind-generic.a linked in.
strip -s %{buildroot}/usr/%{_lib}/libsysprof-*.so
%endif


%check
appstream-util validate-relax --nonet %{buildroot}%{_datadir}/metainfo/*.appdata.xml
desktop-file-validate %{buildroot}%{_datadir}/applications/*.desktop


%files
%license COPYING
%doc NEWS README.md AUTHORS
%{_bindir}/sysprof
%{_datadir}/applications/org.gnome.Sysprof.desktop
%{_datadir}/icons/hicolor/*/*/*
%{_datadir}/metainfo/org.gnome.Sysprof.appdata.xml
%{_datadir}/mime/packages/sysprof-mime.xml

%files agent
%license COPYING
%{_bindir}/sysprof-agent

%files cli -f %{name}.lang
%license COPYING
%{_bindir}/sysprof-cli
%{_libexecdir}/sysprofd
%{_libexecdir}/sysprof-live-unwinder
%{_datadir}/dbus-1/system.d/org.gnome.Sysprof3.conf
%{_datadir}/dbus-1/system-services/org.gnome.Sysprof3.service
%{_datadir}/polkit-1/actions/org.gnome.sysprof3.policy
%{_unitdir}/sysprof3.service

%files -n libsysprof
%license COPYING COPYING.gpl-2
%{_libdir}/libsysprof-6.so.6*
%{_libdir}/libsysprof-memory-6.so
%{_libdir}/libsysprof-speedtrack-6.so
%{_libdir}/libsysprof-tracer-6.so

%files capture-devel
%license src/libsysprof-capture/COPYING
%dir %{_includedir}/sysprof-6
%{_includedir}/sysprof-6/sysprof-address.h
%{_includedir}/sysprof-6/sysprof-capture-condition.h
%{_includedir}/sysprof-6/sysprof-capture-cursor.h
%{_includedir}/sysprof-6/sysprof-capture.h
%{_includedir}/sysprof-6/sysprof-capture-reader.h
%{_includedir}/sysprof-6/sysprof-capture-types.h
%{_includedir}/sysprof-6/sysprof-capture-writer.h
%{_includedir}/sysprof-6/sysprof-clock.h
%{_includedir}/sysprof-6/sysprof-collector.h
%{_includedir}/sysprof-6/sysprof-macros.h
%{_includedir}/sysprof-6/sysprof-platform.h
%{_includedir}/sysprof-6/sysprof-version.h
%{_includedir}/sysprof-6/sysprof-version-macros.h
%{_libdir}/libsysprof-capture-4.a
%{_libdir}/pkgconfig/sysprof-capture-4.pc

%files devel
%{_includedir}/sysprof-6/
%{_libdir}/libsysprof-6.so
%{_libdir}/pkgconfig/sysprof-6.pc
%{_datadir}/dbus-1/interfaces/org.gnome.Sysprof.Agent.xml
%{_datadir}/dbus-1/interfaces/org.gnome.Sysprof3.Profiler.xml
%{_datadir}/dbus-1/interfaces/org.gnome.Sysprof3.Service.xml


%changelog
%autochangelog
