From fb70b977c3ae7d5327621892650f7f42a0c4addd Mon Sep 17 00:00:00 2001
From: Christian Hergert <chergert@redhat.com>
Date: Thu, 10 Oct 2024 17:03:30 -0700
Subject: [PATCH 06/33] libsysprof: add setup hooks for symbolizers

This gives the symbolizer access to the loader so we can propagate tasks
back to it.
---
 src/libsysprof/sysprof-multi-symbolizer.c   | 22 +++++++++++++++++++++
 src/libsysprof/sysprof-symbolizer-private.h |  6 +++++-
 src/libsysprof/sysprof-symbolizer.c         | 11 +++++++++++
 3 files changed, 38 insertions(+), 1 deletion(-)

diff --git a/src/libsysprof/sysprof-multi-symbolizer.c b/src/libsysprof/sysprof-multi-symbolizer.c
index 483cdde5..e1ad90b0 100644
--- a/src/libsysprof/sysprof-multi-symbolizer.c
+++ b/src/libsysprof/sysprof-multi-symbolizer.c
@@ -27,6 +27,7 @@ struct _SysprofMultiSymbolizer
 {
   SysprofSymbolizer parent_instance;
   GPtrArray *symbolizers;
+  guint frozen : 1;
 };
 
 struct _SysprofMultiSymbolizerClass
@@ -138,6 +139,25 @@ sysprof_multi_symbolizer_symbolize (SysprofSymbolizer        *symbolizer,
   return NULL;
 }
 
+static void
+sysprof_multi_symbolizer_setup (SysprofSymbolizer     *symbolizer,
+                                SysprofDocumentLoader *loader)
+{
+  SysprofMultiSymbolizer *self = (SysprofMultiSymbolizer *)symbolizer;
+
+  g_assert (SYSPROF_IS_MULTI_SYMBOLIZER (self));
+  g_assert (SYSPROF_IS_DOCUMENT_LOADER (loader));
+
+  self->frozen = TRUE;
+
+  for (guint i = 0; i < self->symbolizers->len; i++)
+    {
+      SysprofSymbolizer *child = g_ptr_array_index (self->symbolizers, i);
+
+      _sysprof_symbolizer_setup (child, loader);
+    }
+}
+
 static void
 sysprof_multi_symbolizer_finalize (GObject *object)
 {
@@ -159,6 +179,7 @@ sysprof_multi_symbolizer_class_init (SysprofMultiSymbolizerClass *klass)
   symbolizer_class->prepare_async = sysprof_multi_symbolizer_prepare_async;
   symbolizer_class->prepare_finish = sysprof_multi_symbolizer_prepare_finish;
   symbolizer_class->symbolize = sysprof_multi_symbolizer_symbolize;
+  symbolizer_class->setup = sysprof_multi_symbolizer_setup;
 }
 
 static void
@@ -188,6 +209,7 @@ sysprof_multi_symbolizer_take (SysprofMultiSymbolizer *self,
   g_return_if_fail (SYSPROF_IS_MULTI_SYMBOLIZER (self));
   g_return_if_fail (SYSPROF_IS_SYMBOLIZER (symbolizer));
   g_return_if_fail ((gpointer)self != (gpointer)symbolizer);
+  g_return_if_fail (self->frozen == FALSE);
 
   g_ptr_array_add (self->symbolizers, symbolizer);
 }
diff --git a/src/libsysprof/sysprof-symbolizer-private.h b/src/libsysprof/sysprof-symbolizer-private.h
index dd917e44..3d68c52b 100644
--- a/src/libsysprof/sysprof-symbolizer-private.h
+++ b/src/libsysprof/sysprof-symbolizer-private.h
@@ -22,6 +22,7 @@
 
 #include "sysprof-address-layout-private.h"
 #include "sysprof-document.h"
+#include "sysprof-document-loader.h"
 #include "sysprof-mount-namespace-private.h"
 #include "sysprof-process-info-private.h"
 #include "sysprof-strings-private.h"
@@ -41,6 +42,8 @@ struct _SysprofSymbolizerClass
 {
   GObjectClass parent_class;
 
+  void           (*setup)          (SysprofSymbolizer         *self,
+                                    SysprofDocumentLoader     *loader);
   void           (*prepare_async)  (SysprofSymbolizer         *self,
                                     SysprofDocument           *document,
                                     GCancellable              *cancellable,
@@ -56,7 +59,8 @@ struct _SysprofSymbolizerClass
                                     SysprofAddress             address);
 };
 
-
+void           _sysprof_symbolizer_setup          (SysprofSymbolizer         *self,
+                                                   SysprofDocumentLoader     *loader);
 void           _sysprof_symbolizer_prepare_async  (SysprofSymbolizer         *self,
                                                    SysprofDocument           *document,
                                                    GCancellable              *cancellable,
diff --git a/src/libsysprof/sysprof-symbolizer.c b/src/libsysprof/sysprof-symbolizer.c
index 9ad17ca2..47d6021a 100644
--- a/src/libsysprof/sysprof-symbolizer.c
+++ b/src/libsysprof/sysprof-symbolizer.c
@@ -99,3 +99,14 @@ _sysprof_symbolizer_symbolize (SysprofSymbolizer        *self,
 {
   return SYSPROF_SYMBOLIZER_GET_CLASS (self)->symbolize (self, strings, process_info, context, address);
 }
+
+void
+_sysprof_symbolizer_setup (SysprofSymbolizer     *self,
+                           SysprofDocumentLoader *loader)
+{
+  g_return_if_fail (SYSPROF_IS_SYMBOLIZER (self));
+  g_return_if_fail (SYSPROF_IS_DOCUMENT_LOADER (loader));
+
+  if (SYSPROF_SYMBOLIZER_GET_CLASS (self)->setup)
+    SYSPROF_SYMBOLIZER_GET_CLASS (self)->setup (self, loader);
+}
-- 
2.45.2

